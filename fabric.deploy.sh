#!/bin/sh

cd cumulus-ip-fabric && virtualenv VENV && source VENV/bin/activate
pip install -r requirements.txt
ansible-playbook site.yml -i inventory/ipfabric_vagrant 

